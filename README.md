# Demo
[Sample Demo](https://drive.google.com/file/d/1yVToBtgOhjUdIzdwaBENAwXJF1m84XpO/view?usp=sharing)

# Project Description
This project shows a simple web API on the localization using a single resource file with multiple culture.

# Step to Run
1. Rebuild the Project
2. Start Running the Project
3. This web api can be tested by using Postman

